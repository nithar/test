package com.company;

import java.util.ArrayList;
import java.util.Iterator;

public class Quizz {

    String name;
    ArrayList<Question> mesQuestions;

    public Quizz(String name, ArrayList<Question> mesQuestions) {
        this.name = name;
        this.mesQuestions = mesQuestions;

    }



    public void startQuizz(){

        Iterator<Question> it = mesQuestions.iterator();

        while(it.hasNext()){
            boolean checked = false;
            Question currentQuestion = it.next();
            currentQuestion.showQuestionReponses();
            do{
              checked = currentQuestion.setReponse();
            }while(!checked);
        }
    }

}
