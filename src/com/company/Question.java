package com.company;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Question {

    String name;
    ArrayList<Reponse> mesReponses;

    public Question(String name, ArrayList<Reponse> mesReponses) {
        this.name = name;
        this.mesReponses = mesReponses;
    }

    public void showQuestionReponses(){

        System.out.println(name);

        Iterator<Reponse> it = mesReponses.iterator();

        while(it.hasNext()){

            Reponse currentResponse = it.next();
            currentResponse.showReponse();

        }



    }

    public boolean setReponse()  {
        boolean checked = false;
            try {
                DataInputStream is = new DataInputStream(System.in);
                Integer choosenResponse = Integer.parseInt(is.readLine());
                testReponse(choosenResponse-1);
                checked = true ;
            }catch(IOException e){
                System.out.println("Erreur de saisie !");
            } catch(NumberFormatException e){
                System.out.println("Veuillez saisir un nombre !");
            }catch(IndexOutOfBoundsException e){
                System.out.println("Cette reponse est indisponible !");
            }

        return checked;


    }

    public void testReponse(Integer choosenResponse){
       if(mesReponses.get(choosenResponse).isGood){
            System.out.println("Bonne reponse !");
       }else {
           System.out.println("Mauvaise reponse !");
       }

    }
}
