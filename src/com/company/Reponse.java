package com.company;

public class Reponse {

    String reponse;
     boolean isGood;

    public Reponse(String reponse, boolean isGood) {
        this.reponse = reponse;
        this.isGood = isGood;
    }

    public void showReponse(){
        System.out.println(reponse);
    }
}
